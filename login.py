from requests import Session
from bs4 import BeautifulSoup
from time import sleep

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.134 YaBrowser/22.7.1.802 Yowser/2.5 Safari/537.36",
    "X-Amzn-Trace-Id": "Root=1-62e7c359-39d5cdc10e79f756776998ab"
  }

work = Session()

work.get("http://quotes.toscrape.com/", headers=headers)

response = work.get("http://quotes.toscrape.com/login", headers=headers)

soup = BeautifulSoup(response.text, "lxml") #html.parser

token = soup.find("form").find("input").get("value")

data = {"csrf_token": token, "username": "noname", "password": "password"}

result = work.post("http://quotes.toscrape.com/login", headers=headers, data=data, allow_redirects=True)

soup2 = BeautifulSoup(result.text, "lxml") #html.parser

result2 = soup2.find_all("span", class_="text")
author = soup2.find_all("small", class_="author")

for i in result2:

  if len(result2) != 0:
    # quote1 = i.find("span", class_="text")
    print(i.text)
  else:
    break